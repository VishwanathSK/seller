package com.example.taskthorsignia;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.taskthorsignia.model.BusinessResponse;
import com.example.taskthorsignia.network.APIInterface;
import com.example.taskthorsignia.network.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessDetails extends AppCompatActivity {

    TextInputEditText ownername,shopename,passwords,confpass,building,road,city,state,landmark,logo;
    RadioGroup acctype;
    RadioButton grower,wholesaler,reseller;
    String accounttype="grower";
    static final Integer READ_EXST = 0;
    AppCompatButton savenext;
    Context context;
    Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_details);
        context= this;
        setUi();


        acctype.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.grower:
                        // do operations specific to this selection
                        accounttype = "grower";
                        break;
                    case R.id.wholesaler:
                        // do operations specific to this selection
                        accounttype = "wholesaler";
                        break;
                    case R.id.reseller:
                        // do operations specific to this selection
                        accounttype = "reseller";
                        break;
                }
            }
        });


        savenext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ownername.getText().toString().trim().isEmpty()
                        && !shopename.getText().toString().trim().isEmpty()
                        && !passwords.getText().toString().trim().isEmpty()
                        && !confpass.getText().toString().trim().isEmpty()
                        && !building.getText().toString().trim().isEmpty()
                        && !road.getText().toString().trim().isEmpty()
                        && !city.getText().toString().trim().isEmpty()
                        && !state.getText().toString().trim().isEmpty()
                        && !landmark.getText().toString().trim().isEmpty()
                        && !logo.getText().toString().trim().isEmpty()
                        && passwords.getText().toString().trim().equals(confpass.getText().toString().trim())

                ) {

                    uploadData();
                }
                else{
                    Toast.makeText(context,"fill all details ,match password and confirm password ",Toast.LENGTH_LONG).show();
                }
            }
        });

        AppCompatImageView gallery = findViewById(R.id.gallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                askForPermission(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},READ_EXST);

            }
        });

    }

    private void askForPermission(String[] permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(BusinessDetails.this, permission[0]) != PackageManager.PERMISSION_GRANTED
        && ContextCompat.checkSelfPermission(BusinessDetails.this, permission[1]) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(BusinessDetails.this, permission[0]) &&
                    ActivityCompat.shouldShowRequestPermissionRationale(BusinessDetails.this, permission[1]))
            {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(BusinessDetails.this, new String[]{permission[0],permission[1]}, requestCode);


            } else {

                ActivityCompat.requestPermissions(BusinessDetails.this, new String[]{permission[0],permission[1]}, requestCode);


            }
        } else {

            Intent imageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(imageIntent, 11);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode==RESULT_OK && requestCode==11){
            fileUri= data.getData();
            logo.setText(fileUri.toString());
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode==READ_EXST){
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    Intent imageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(imageIntent, 11);
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                }  else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return;
        }
        // Other 'case' lines to check for other
        // permissions this app might request.
    }


    private void uploadData() {


        // create upload service client
        APIInterface service =
                RetrofitInstance.getClient().create(APIInterface.class);

// create part for file (photo, video, ...)
        MultipartBody.Part filebody = prepareFilePart("seller_logo", fileUri);

// create a map of data to pass along
        //RequestBody seller_code = createPartFromString(Util.getSellerUID(context));
        RequestBody seller_code = createPartFromString(Util.getSellerUID(context));
        RequestBody owner_name = createPartFromString(ownername.getText().toString().trim());
        RequestBody company_name = createPartFromString(shopename.getText().toString().trim());
        RequestBody seller_password = createPartFromString(passwords.getText().toString().trim());
        RequestBody seller_street = createPartFromString(building.getText().toString().trim());
        RequestBody seller_area = createPartFromString(road.getText().toString().trim());
        RequestBody seller_city = createPartFromString(city.getText().toString().trim());
        RequestBody seller_state = createPartFromString(state.getText().toString().trim());
        RequestBody seller_landmark = createPartFromString(landmark.getText().toString().trim());
        RequestBody account_type = createPartFromString(accounttype);

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("seller_code", seller_code);
        map.put("owner_name", owner_name);
        map.put("company_name", company_name);
        map.put("seller_password", seller_password);
        map.put("seller_street", seller_street);
        map.put("seller_area", seller_area);
        map.put("seller_city", seller_city);
        map.put("seller_state", seller_state);
        map.put("seller_landmark", seller_landmark);
        map.put("account_type", account_type);

        Call<BusinessResponse> call  = service.businessData(map,filebody);
        call.enqueue(new Callback<BusinessResponse>() {
            @Override
            public void onResponse(Call<BusinessResponse> call, Response<BusinessResponse> response) {
                if(response.isSuccessful()){
                    Log.i("main","hiii");
                    BusinessResponse  businessResponse = response.body();
                    Log.i("main","hi "+businessResponse.toString());
                    Intent intent = new Intent(BusinessDetails.this,BankDetails.class);
                    startActivity(intent);
                }

            }

            @Override
            public void onFailure(Call<BusinessResponse> call, Throwable t) {
                Toast.makeText(context, "problem uploading data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUi(){
        ownername = findViewById(R.id.ownername);
        shopename = findViewById(R.id.shopname);
        passwords = findViewById(R.id.password);
        confpass = findViewById(R.id.confirm_password);
        building = findViewById(R.id.building);
        road = findViewById(R.id.road);
        city = findViewById(R.id.city);
        state = findViewById(R.id.state);
        landmark = findViewById(R.id.landmark);
        logo = findViewById(R.id.logo);
        savenext = findViewById(R.id.savenext);

        acctype = findViewById(R.id.radio_group);
        grower = findViewById(R.id.grower);
        wholesaler = findViewById(R.id.wholesaler);
        reseller = findViewById(R.id.reseller);

    }

    private RequestBody createPartFromString(String s) {

        RequestBody description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, s);

        return description;
    }


    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri


        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor c = getContentResolver().query(fileUri, filePath,
                null, null, null);
        c.moveToFirst();
        int columnIndex = c.getColumnIndex(filePath[0]);
        String FilePathStr = c.getString(columnIndex);
        c.close();


        File file;
        //file = FileUtils.getFile(this, fileUri);
        file=new File(FilePathStr);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getContentResolver().getType(fileUri)),
                        file
                );
        //RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);


    }


}