package com.example.taskthorsignia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.taskthorsignia.model.RegResponse;
import com.example.taskthorsignia.network.APIInterface;
import com.example.taskthorsignia.network.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    AppCompatEditText mobilenum;
    AppCompatButton login;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mobilenum = findViewById(R.id.mobilenum);
        login = findViewById(R.id.login);
        context = this;
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobilenumber = mobilenum.getText().toString().trim();
                if(mobilenumber!=null && !mobilenumber.isEmpty())
                {
                        regNumber(mobilenumber);
                }
            }
        });
    }

    private void regNumber(final String mobilenumber) {
        Log.i("main","res "+mobilenumber);
        APIInterface apiInterface = RetrofitInstance.getClient().create(APIInterface.class);
        Call<RegResponse> sellercode = apiInterface.regMobile(mobilenumber);
        sellercode.enqueue(new Callback<RegResponse>() {
            @Override
            public void onResponse(Call<RegResponse> call, Response<RegResponse> response) {
                if(response.isSuccessful()){

                    Util.setSellerUID(response.body().getSelleruniqueid().toString(),context);
                            Log.i("main","res "+response.body().getSelleruniqueid());
                            //mobilenum.setText(response.body().getSelleruniqueid().toString());
                            Intent intent = new Intent(MainActivity.this,ValidatingMobile.class);
                            intent.putExtra("mobilenum",mobilenumber);
                            startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<RegResponse> call, Throwable t) {
                Log.i("main","res err  "+t.getMessage());
                Toast.makeText(context, "something went wrong try again", Toast.LENGTH_SHORT).show();
            }
        });

    }
}