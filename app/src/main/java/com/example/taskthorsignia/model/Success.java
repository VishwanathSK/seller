package com.example.taskthorsignia.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

    public class Success {

        @SerializedName("seller_code")
        @Expose
        private String sellerCode;
        @SerializedName("owner_name")
        @Expose
        private String ownerName;
        @SerializedName("company_name")
        @Expose
        private String companyName;
        @SerializedName("seller_email")
        @Expose
        private Object sellerEmail;
        @SerializedName("seller_password")
        @Expose
        private String sellerPassword;
        @SerializedName("seller_street")
        @Expose
        private String sellerStreet;
        @SerializedName("seller_area")
        @Expose
        private String sellerArea;
        @SerializedName("seller_city")
        @Expose
        private String sellerCity;
        @SerializedName("seller_state")
        @Expose
        private String sellerState;
        @SerializedName("seller_landmark")
        @Expose
        private String sellerLandmark;
        @SerializedName("seller_logo")
        @Expose
        private String sellerLogo;
        @SerializedName("account_type")
        @Expose
        private String accountType;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getSellerCode() {
            return sellerCode;
        }

        public void setSellerCode(String sellerCode) {
            this.sellerCode = sellerCode;
        }

        public String getOwnerName() {
            return ownerName;
        }

        public void setOwnerName(String ownerName) {
            this.ownerName = ownerName;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public Object getSellerEmail() {
            return sellerEmail;
        }

        public void setSellerEmail(Object sellerEmail) {
            this.sellerEmail = sellerEmail;
        }

        public String getSellerPassword() {
            return sellerPassword;
        }

        public void setSellerPassword(String sellerPassword) {
            this.sellerPassword = sellerPassword;
        }

        public String getSellerStreet() {
            return sellerStreet;
        }

        public void setSellerStreet(String sellerStreet) {
            this.sellerStreet = sellerStreet;
        }

        public String getSellerArea() {
            return sellerArea;
        }

        public void setSellerArea(String sellerArea) {
            this.sellerArea = sellerArea;
        }

        public String getSellerCity() {
            return sellerCity;
        }

        public void setSellerCity(String sellerCity) {
            this.sellerCity = sellerCity;
        }

        public String getSellerState() {
            return sellerState;
        }

        public void setSellerState(String sellerState) {
            this.sellerState = sellerState;
        }

        public String getSellerLandmark() {
            return sellerLandmark;
        }

        public void setSellerLandmark(String sellerLandmark) {
            this.sellerLandmark = sellerLandmark;
        }

        public String getSellerLogo() {
            return sellerLogo;
        }

        public void setSellerLogo(String sellerLogo) {
            this.sellerLogo = sellerLogo;
        }

        public String getAccountType() {
            return accountType;
        }

        public void setAccountType(String accountType) {
            this.accountType = accountType;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }

