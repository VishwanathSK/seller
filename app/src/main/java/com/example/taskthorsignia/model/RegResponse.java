package com.example.taskthorsignia.model;

 import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

    public class RegResponse {

        @SerializedName("success")
        @Expose
        private String success;
        @SerializedName("info")
        @Expose
        private List<String> info = null;
        @SerializedName("selleruniqueid")
        @Expose
        private String selleruniqueid;

        public String getSuccess() {
            return success;
        }

        public void setSuccess(String success) {
            this.success = success;
        }

        public List<String> getInfo() {
            return info;
        }

        public void setInfo(List<String> info) {
            this.info = info;
        }

        public String getSelleruniqueid() {
            return selleruniqueid;
        }

        public void setSelleruniqueid(String selleruniqueid) {
            this.selleruniqueid = selleruniqueid;
        }

    }
