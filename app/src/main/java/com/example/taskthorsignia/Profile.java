package com.example.taskthorsignia;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        TextInputEditText phone = findViewById(R.id.phonenumber);
        phone.setText(Util.getSellerPhone(this));
    }
}