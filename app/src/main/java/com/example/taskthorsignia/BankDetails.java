package com.example.taskthorsignia;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.taskthorsignia.model.OtpResponse;
import com.example.taskthorsignia.network.APIInterface;
import com.example.taskthorsignia.network.RetrofitInstance;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankDetails extends AppCompatActivity {

    TextInputEditText accname,accnumber,reaccnum,ifsc,bankname,city,cheque;
    AppCompatImageView gallerybutton;
    AppCompatButton save;
    private Uri fileUri;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);
        context = this;
        setUi();
        gallerybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent imageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(imageIntent, 11);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!accname.getText().toString().trim().isEmpty()
                && !accnumber.getText().toString().trim().isEmpty()
                && !reaccnum.getText().toString().trim().isEmpty()
                && !ifsc.getText().toString().trim().isEmpty()
                && !bankname.getText().toString().trim().isEmpty()
                && !city.getText().toString().trim().isEmpty()
                && !cheque.getText().toString().trim().isEmpty()
                && accnumber.getText().toString().trim().equals(reaccnum.getText().toString().trim())
                ){
                    uploadData();
                }
                else{
                    Toast.makeText(context,"fill all details ,match Account number and Retype Account Number ",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void uploadData() {



        // create upload service client
        APIInterface service =
                RetrofitInstance.getClient().create(APIInterface.class);

// create part for file (photo, video, ...)
        MultipartBody.Part filebody = prepareFilePart("cancelled_cheque", fileUri);

// create a map of data to pass along
        //RequestBody seller_code = createPartFromString(Util.getSellerUID(context));
        RequestBody seller_code = createPartFromString(Util.getSellerUID(context));
        RequestBody account_name = createPartFromString(accname.getText().toString().trim());
        RequestBody account_number = createPartFromString(accnumber.getText().toString().trim());
        RequestBody branch_ifsccode = createPartFromString(ifsc.getText().toString().trim());
        RequestBody bank_name = createPartFromString(bankname.getText().toString().trim());
        RequestBody branch_name = createPartFromString(city.getText().toString().trim());


        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("seller_code", seller_code);
        map.put("account_name", account_name);
        map.put("account_number", account_number);
        map.put("branch_ifsccode", branch_ifsccode);
        map.put("bank_name", bank_name);
        map.put("branch_name", branch_name);

        Call<OtpResponse> call = service.bankData(map,filebody);
        call.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                if(response.isSuccessful()){
                    Log.i("main","hello ="+response.body().getSuccess().toString());
                    Intent intent = new Intent(BankDetails.this,Profile.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                Log.i("main","err "+t.getMessage());
                Toast.makeText(context, "problem uploading data", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void setUi() {
        accname = findViewById(R.id.accountname);
        accnumber = findViewById(R.id.accountnumber);
        reaccnum = findViewById(R.id.reaccountnumber);
        ifsc = findViewById(R.id.ifsccode);
        bankname = findViewById(R.id.bankname);
        city = findViewById(R.id.city_bank);
        cheque = findViewById(R.id.cheque);
        gallerybutton = findViewById(R.id.galley_bank);
        save = findViewById(R.id.savenext_bank);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode==RESULT_OK && requestCode==11){
            fileUri= data.getData();
            cheque.setText(fileUri.toString());
        }
        super.onActivityResult(requestCode, resultCode, data);

    }


    private RequestBody createPartFromString(String s) {

        RequestBody description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, s);

        return description;
    }


    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri


        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor c = getContentResolver().query(fileUri, filePath,
                null, null, null);
        c.moveToFirst();
        int columnIndex = c.getColumnIndex(filePath[0]);
        String FilePathStr = c.getString(columnIndex);
        c.close();


        File file;
        //file = FileUtils.getFile(this, fileUri);
        file=new File(FilePathStr);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);


    }
}