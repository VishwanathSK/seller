package com.example.taskthorsignia.network;


import com.example.taskthorsignia.model.BusinessResponse;
import com.example.taskthorsignia.model.OtpResponse;
import com.example.taskthorsignia.model.RegResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface APIInterface {


    @FormUrlEncoded
    @POST("sellermobile/reg")
    Call<RegResponse> regMobile(@Field("seller_mobile") String mobile);


    @FormUrlEncoded
    @POST("sellermobile/verify")
    Call<OtpResponse> otpMobile(@Field("seller_code") String sellercode, @Field("seller_otp") String otp);

    @Multipart
    @POST("store/seller")
    Call<BusinessResponse> businessData(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part file);


    @Multipart
    @POST("saveseller/bank")
    Call<OtpResponse> bankData(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part file);

}
