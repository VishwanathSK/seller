package com.example.taskthorsignia.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitInstance {

    static String API_BASE_URL = "http://omyguru.com/flowerbazaar/public/api/";

    static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            /*.callTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(40, TimeUnit.SECONDS)
            .readTimeout(40, TimeUnit.SECONDS)
            .writeTimeout(40, TimeUnit.SECONDS);*/
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        if(retrofit==null){

            Retrofit.Builder builder =
                    new Retrofit.Builder()
                            .baseUrl(API_BASE_URL)
                            .addConverterFactory(
                                    GsonConverterFactory.create()
                            );

             retrofit =
                    builder
                            .client(
                                    httpClient.build()
                            )
                            .build();

        }

        return retrofit;
    }
}
