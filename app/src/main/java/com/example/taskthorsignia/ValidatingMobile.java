package com.example.taskthorsignia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.taskthorsignia.model.OtpResponse;
import com.example.taskthorsignia.model.RegResponse;
import com.example.taskthorsignia.network.APIInterface;
import com.example.taskthorsignia.network.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ValidatingMobile extends AppCompatActivity {


    AppCompatTextView mobnumber;
    AppCompatEditText otp;
    AppCompatButton verify;
    Context context;
    String mob="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validating_mobile);
        mobnumber = findViewById(R.id.idnumber);
        otp = findViewById(R.id.otp);
        verify = findViewById(R.id.verify);
         mob= getIntent().getStringExtra("mobilenum");
        if(mob!=null){
            mobnumber.setText("Please type verification code sent to +91 "+mob);
        }

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  otpstring = otp.getText().toString().trim();
                if(otpstring!=null && !otpstring.isEmpty()){
                    sentToVerify(otpstring);
                }
            }
        });


    }

    private void sentToVerify(String otpstring) {


        APIInterface apiInterface = RetrofitInstance.getClient().create(APIInterface.class);
        Call<OtpResponse> otpcode = apiInterface.otpMobile(Util.getSellerUID(context),otpstring);
        otpcode.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                if(response.isSuccessful()){

                    if(response.body().getSuccess().equals("Succesfully verified the OTP")){

                        //Toast.makeText(context,"verification successfully done",Toast.LENGTH_SHORT).show();
                        Util.setSellerPhone(mob,context);
                        Intent intent = new Intent(ValidatingMobile.this,BusinessDetails.class);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                Toast.makeText(context," something went wrong try again",Toast.LENGTH_SHORT).show();

            }
        });

    }
}