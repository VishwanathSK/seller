package com.example.taskthorsignia;

import android.content.Context;
import android.content.SharedPreferences;

public class Util {

    public static SharedPreferences sharedPreferences;
    public static String SUID = "SELLERUID";
    public static String SPHONE = "SELLERPHONE";

            public static void setSellerUID(String suid, Context context){

                if(sharedPreferences==null){
                    sharedPreferences = context.getSharedPreferences(SUID,Context.MODE_PRIVATE);
                }
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("seller_uid",suid);
                editor.commit();
            }

            public static String getSellerUID(Context context){
                if(sharedPreferences==null){
                    sharedPreferences = context.getSharedPreferences(SUID,Context.MODE_PRIVATE);
                }
                return sharedPreferences.getString("seller_uid",null);
            }


    public static void setSellerPhone(String phone, Context context){

        if(sharedPreferences==null){
            sharedPreferences = context.getSharedPreferences(SPHONE,Context.MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("seller_phone",phone);
        editor.commit();
    }

    public static String getSellerPhone(Context context){
        if(sharedPreferences==null){
            sharedPreferences = context.getSharedPreferences(SPHONE,Context.MODE_PRIVATE);
        }
        return sharedPreferences.getString("seller_phone",null);
    }

}
